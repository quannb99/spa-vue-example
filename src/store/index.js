import Vue from "vue"
import Vuex from "vuex"
import EventService from "@/services/EventService.js"
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: { id: "123456", name: "Quan Nguyen" },
    categories: [
      "sustainability",
      "nature",
      "animal welfare",
      "housing",
      "education",
      "food",
      "community",
    ],
    events: [],
    eventsTotal: 0,
    event: {},
  },
  mutations: {
    ADD_EVENT(state, event) {
      state.events.push(event)
    },
    SET_EVENTS(state, events) {
      state.events = events
    },
    SET_EVENTS_TOTAL(state, totalEvent) {
      state.eventsTotal = totalEvent
    },
    SET_EVENT(state, event) {
      state.event = event
    },
    DELELTE_EVENT(state, id) {
      state.events = state.events.filter((event) => event.id !== id)
    },
  },
  actions: {
    createEvent({ commit }, event) {
      return EventService.postEvent(event).then(() => {
        commit("ADD_EVENT", event)
      })
    },

    fetchEvents({ commit }, { perPage, page }) {
      EventService.getEvents(perPage, page)
        .then((response) => {
          commit("SET_EVENTS", response.data)
          commit("SET_EVENTS_TOTAL", response.headers["x-total-count"])
        })
        .catch((error) => {
          console.log("Error: " + error.message)
        })
    },

    fetchEvent({ commit, getters }, id) {
      var event = getters.getEventById(id)
      if (event) {
        commit("SET_EVENT", event)
      } else {
        EventService.getEvent(id)
          .then((response) => {
            commit("SET_EVENT", response.data)
          })
          .catch((error) => {
            console.log("Error: " + error.message)
          })
      }
    },

    updateEvent({ commit }, event) {
      commit
      return EventService.putEvent(event)
    },

    deleteEvent({ commit }, id) {
      return EventService.deleteEvent(id)
        .then(() => {
          commit("DELELTE_EVENT", id)
        })
        .catch((error) => {
          console.log("Error: " + error.message)
        })
    },
  },
  getters: {
    getEventById: (state) => (id) => {
      return state.events.find((event) => event.id === id)
    },
  },
  modules: {},
})
